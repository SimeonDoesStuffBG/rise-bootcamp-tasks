﻿using OOPNatureReserveSimulationSolution.Animals;
using OOPNatureReserveSimulationSolution.Plants;
using OOPNatureReserveSimulationSolution.OtherFoods;

namespace OOPNatureReserveSimulationSolution
{
    public class BiosphereSimulation
    {
        private int size;
        private Tile[] map;

        private Random random;

        private string InfoForATurn = "";
        private bool shouldPprintDetails;

        public BiosphereSimulation(int size)
        {
            this.size = size;
            int tiles = size * size;
            map = new Tile[tiles];
            for (int i = 0; i < tiles; i++)
            {
                int x = i / size;
                int y = i % size;

                map[i] = Tile.CreateTileAt(x, y);
            }
        }

        //private Food GetRandomPieceOfFood()
        //{
        //    int food = random.Next(Enum.GetNames(typeof(Food)).Length - 1);
        //    GenerateFoodInRange(Food.plantsStart, Food.plantsEnd);
        //    return (Food)food;
        //}

        //private Food GenerateFoodInRange(Food startFood, Food endFood)
        //{
        //    var foodIndex = random.Next((int)startFood - (int)endFood) + (int)startFood;

        //    return (Food)foodIndex;
        //}

        public void SimulateBiosphere()
        {
            int livingAnimals = 0;
            int currentTurn = 0;
            do
            {
                MoveAnimalsAcrossTheMap();

                SimulateTilesTurn();

                SetCurrentTurnInformation(++currentTurn);
                Console.WriteLine(InfoForATurn);

                livingAnimals = GetLivingAnimals();

            } while (livingAnimals > 0);
        }

        private void SimulateTilesTurn()
        {
            foreach (Tile tile in map)
            {
                tile.SimulateTurn();
            }
        }

        private void MoveAnimalsAcrossTheMap()
        {
            foreach (Tile tile in map)
            {
                tile.MoveAnimalsToNeighbours(GetTileNeighbours(tile.x, tile.y));
            }
        }


        private void SetCurrentTurnInformation(int curentTurn)
        {

            InfoForATurn = $"Turn {curentTurn}:\n";

            int livingAnimals = GetLivingAnimals();
            int deadAnimals = GetDeadAnimals();

            InfoForATurn += $"We have {livingAnimals} living and {deadAnimals} dead animals.\n";
        }

        private int GetLivingAnimals() 
            => map.Sum(tile => tile.livingAnimals);
        //{
        //    return map.Sum(tile => tile.livingAnimals);

        ////    foreach(Tile tile in map)
        ////    {
        ////        livingAnimals += tile.livingAnimals;
        ////    }
        ////    return livingAnimals;
        //}

        private int GetDeadAnimals()
            => map.Sum(tile => tile.deadAnimals);

        //{

        //    return map.Sum(tile => tile.deadAnimals);
        //    //int deadAnimals = 0;

        //    //foreach(Tile tile in map)
        //    //{
        //    //    deadAnimals += tile.deadAnimals;
        //    //}

        //    //return deadAnimals;
        //}

        public (int minLife, int averageLife, int maxLife) BiosphereStatistic()
        {
            (int minLife, int averageLife, int maxLife) stats = (int.MaxValue, 0, 0);

            foreach (Tile tile in map)
            {
                var tileStats = tile.TileStats();

                if (tileStats.minLife < stats.minLife)
                {
                    stats.minLife = tileStats.minLife;
                }

                if (tileStats.maxLife > stats.maxLife)
                {
                    stats.maxLife = tileStats.maxLife;
                }

                stats.averageLife += tileStats.averageLife;
            }

            stats.averageLife /= map.Length;
            return stats;
        }

        private int GetIndexFromPosition(int x, int y)
        {
            return x * size + y;
        }

        private Tile[] GetTileNeighbours(int x, int y)
        {
            Tile[] neighbours = new Tile[8];
            int index = 0;
            for (int i = -1; i <= 1; i++)
            {
                int newX = (size + x + i) % size;
                for (int j = -1; j <= 1; j++)
                {
                    if (i == 0 && i == j)
                    {
                        continue;
                    }
                    int newY = (size + y + j) % size;
                    int tileIndex = GetIndexFromPosition(newX, newY);
                    neighbours[index] = map[tileIndex];
                    index++;
                }
            }
            return neighbours;
        }
    }
}
