﻿// See https://aka.ms/new-console-template for more information
using OOPNatureReserveSimulationSolution;

Console.WriteLine("Hello, World!");

BiosphereSimulation simulation = new BiosphereSimulation(10);
simulation.SimulateBiosphere();

var simulationStats = simulation.BiosphereStatistic();

Console.WriteLine($"Min Life: {simulationStats.minLife} life units");
Console.WriteLine($"Max Life: {simulationStats.maxLife} life units");
Console.WriteLine($"Average Life: {simulationStats.averageLife}  life units");