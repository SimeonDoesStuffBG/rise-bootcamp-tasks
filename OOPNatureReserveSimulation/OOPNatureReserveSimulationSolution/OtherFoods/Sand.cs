﻿namespace OOPNatureReserveSimulationSolution.OtherFoods
{
    internal class Sand : Edible
    {
        public Sand() : base(13, Food.sand) { }
    }
}
