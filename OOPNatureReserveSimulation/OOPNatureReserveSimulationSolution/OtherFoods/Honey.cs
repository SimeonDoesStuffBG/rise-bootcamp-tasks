﻿namespace OOPNatureReserveSimulationSolution.OtherFoods
{
    internal class Honey : Edible
    {
        public Honey() : base(12, Food.honey) { }
    }
}
