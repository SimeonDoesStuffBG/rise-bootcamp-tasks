﻿namespace OOPNatureReserveSimulationSolution.OtherFoods
{
    internal class Blood : Edible
    {
        public Blood() : base(21, Food.blood) { }
    }
}
