﻿using OOPNatureReserveSimulationSolution.Animals;
using OOPNatureReserveSimulationSolution.Plants;
using OOPNatureReserveSimulationSolution.Biomes;

namespace OOPNatureReserveSimulationSolution
{
    internal class Tile
    {
        public readonly int x;
        public readonly int y;
        public int livingAnimals { get; private set; }
        public int deadAnimals { get => animals.Count - livingAnimals; }
        private static Random random = new Random();
        private static readonly Func<int, int, Tile>[] tileFactories =
        {
            (int x,int y) => new Tile(x,y, Desert.Instance())
            //TODO Add these:
            //Forest
            //Ocean
            //planes
            //Farm
        };

        private readonly Biome biome;

        private List<Animal> animals = new List<Animal>();
        private List<Plant> plants = new List<Plant>();
        private List<Edible> foods = new List<Edible>();

        public Tile(int x, int y, Biome biome)
        {
            this.x = x;
            this.y = y;
            this.biome = biome;
            //this.animals = GenerateAnimals();
            //this.plants = GeneratePlants();
            //this.foods = GenerateOther().Concat(this.animals).Concat(this.plants).ToList();
            FillAnimalList();
            FillPlantList();
            FillOtherFoodList();
            livingAnimals = animals.Count;
        }

        public static Tile CreateTileAt(int x, int y)
        {
            int index = random.Next(tileFactories.Length);

            return tileFactories[index](x, y);
        }

        

        public void MoveAnimalTo(Animal animal, Tile destination)
        {
            if (animal.IsAlive() && destination.biome.AnimalLivesHere(animal))
            {
                this.RemoveAnimal(animal);
                destination.AddAnimal(animal);
            }
        }
        
        private void AddAnimal(Animal animal)
        {
            this.animals.Add(animal);
            this.foods.Add(animal);
        }

        private void RemoveAnimal(Animal animal)
        {
            this.animals.Remove(animal);
            this.foods.Remove(animal);
        }

        private void FillAnimalList()
        {
            for(int i =0; i<biome.animals; i++)
            {
                Animal animal = biome.CreateAnimal();

                animals.Add(animal);
                foods.Add(animal);
            }
        }

        private void FillPlantList()
        {
            for(int i = 0; i<biome.plants; i++)
            {
                Plant plant = biome.CreatePlant();

                plants.Add(plant);
                foods.Add(plant);
            }
        }

        private void FillOtherFoodList()
        {
            for(int i=0; i<biome.otherFoods; i++)
            {
                Edible food = biome.CreateOtherFood();

                foods.Add(food);
            }
        }

        public void MoveAnimalsToNeighbours(Tile[] neighbours)
        {
            var animalsCopy = animals.ToArray();//I hate this
            foreach(Animal animal in animalsCopy)
            {
                int destinationIndex = random.Next(neighbours.Length);
                MoveAnimalTo(animal, neighbours[destinationIndex]);
            }
        }

        public void SimulateTurn()
        {
            int aliveAnimals = 0;
            foreach(Animal animal in animals)
            {
                if (animal.IsAlive())
                {
                    aliveAnimals++;

                    FeedAnimal(animal);
                }

                foreach (Plant plant in plants)
                {
                    plant.Regenerate();
                }
            }
            livingAnimals = aliveAnimals;
        }

        private void FeedAnimal(Animal animal)
        {
            var foodData = FindFood(animal);

            animal.Feed(foodData.food);

            if (!foodData.food.HasNutrients())
            {
                foods.RemoveAt(foodData.foodIndex);
            }
        }

        private (int foodIndex, Edible food) FindFood(Animal animal)
        {
            int foodIndex;
            Edible food;
            do
            {
                foodIndex = random.Next(foods.Count);
                food = foods[foodIndex];
            } while (food == animal);
            return (foodIndex, food);
        }

        public (int minLife, int maxLife, int averageLife) TileStats()
        {
            (int minLife, int maxLife, int averageLife) stats = (int.MaxValue, 0, 0);
            
            foreach(Animal animal in animals)
            {
                if(animal.LifeSpan < stats.minLife)
                {
                    stats.minLife = animal.LifeSpan;
                }

                if(animal.LifeSpan > stats.maxLife)
                {
                    stats.maxLife = animal.LifeSpan;
                }

                stats.averageLife += animal.LifeSpan;
            }
            if (animals.Count != 0)
            {
                stats.averageLife /= animals.Count;
            }
            return stats;
        }
    }
}
