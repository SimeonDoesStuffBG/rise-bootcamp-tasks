﻿namespace OOPNatureReserveSimulationSolution.Animals
{
    internal class Frog : Animal
    {
        private static HashSet<Food> tadpoleDiet = new() { Food.grass, Food.strawberry, Food.algae };
        private static HashSet<Food> adultFrogDiet = new(){ Food.frog, Food.mosquito, Food.fly };
        private static int adulthood = 12;
        public Frog() : base(6, Food.frog, 13, 1) { }

        protected override bool CanEat(Food food)
        {
            if (LifeSpan < adulthood)
            {
                return FoodIsContainedInDiet(food, tadpoleDiet);
            }

            return FoodIsContainedInDiet(food, adultFrogDiet);
        }
    }
}
