﻿namespace OOPNatureReserveSimulationSolution.Animals
{
    
    internal class Cat : Animal
    {
        private static HashSet<Food> mondayDiet = new() { Food.wabbit, Food.beef };
        private static HashSet<Food> regularDiet = new() { Food.fish, Food.mutton };

        public Cat() : base(0, Food.cat, 21, 3) { }

        protected override bool CanEat(Food food)
        {
            return (LifeSpan % 7 == 1 && FoodIsContainedInDiet(food, mondayDiet)) || (LifeSpan % 7 != 1 && FoodIsContainedInDiet(food, regularDiet));
        }
    }
}
