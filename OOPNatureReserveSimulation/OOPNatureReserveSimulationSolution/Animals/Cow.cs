﻿namespace OOPNatureReserveSimulationSolution.Animals
{
    internal class Cow:Animal
    {
        private static HashSet<Food> initialDiet = new() { Food.grass };
        private static HashSet<Food> adultDiet = new() { Food.apple, Food.leaves, Food.blueberry, Food.strawberry };
        
        int adulthood = 6;
        public Cow() : base(14, Food.beef,20, 2) { }

        protected override bool CanEat(Food food)
        {
            return FoodIsContainedInDiet(food, initialDiet) || (LifeSpan >= adulthood && FoodIsContainedInDiet(food, adultDiet));
        }

    }
}
