﻿namespace OOPNatureReserveSimulationSolution.Animals
{
    internal class Bear : Animal
    {
        private static HashSet<Food> regularDiet = new() { Food.honey, Food.apple, Food.fish, Food.strawberry, Food.rose };
        private static HashSet<Food> starvationDiet = new() { Food.pork, Food.mutton, Food.beef, Food.wabbit };

        public Bear() : base(0, Food.bear, 45, 5, 5) { }

        protected override bool CanEat(Food food)
        {
            return FoodIsContainedInDiet(food, regularDiet) || (IsStarving() && FoodIsContainedInDiet(food, starvationDiet));
        }
    }
}
