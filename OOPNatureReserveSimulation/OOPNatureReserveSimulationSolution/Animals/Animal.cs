﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("OOPNatureReserveSimulationTests")]
namespace OOPNatureReserveSimulationSolution.Animals
{
    internal abstract class Animal : Edible
    {
        private readonly int maxEnergy;
        protected int currentEnergy;
        protected readonly int energyLoss;
        protected readonly int starvationLevel;
        private bool isEaten = false;
       
        private int lifeSpan = 0;

        public int LifeSpan { get => lifeSpan; }

        public string Message { get; protected set; }
        public Animal(int maxNutrients, Food type, int maxEnergy, int energyLoss, int starvationLevel=0):base(maxNutrients, type)
        {
            this.maxEnergy = maxEnergy;
            this.currentEnergy = maxEnergy;
            this.starvationLevel = starvationLevel;
            this.energyLoss = energyLoss;
        }

        protected static bool FoodIsContainedInDiet(Food food, HashSet<Food> diet)
        {
            return diet.Contains(food);
        }

        protected abstract bool CanEat(Food food);

        public virtual void Feed(Edible food)
        {
            if (IsAlive())
            {
                Message = this.GetType().Name + " ";

                if (CanEat(food.type) && food.CurrentNutrients > 0)
                {
                    Message += $"has eaten {food.type}";
                    int energyGained = Math.Min(food.CurrentNutrients, maxEnergy - currentEnergy);
                    currentEnergy += food.GetEaten(energyGained);
                }
                else
                {
                    Message += $"cannot eat {food.type}";
                    currentEnergy -= energyLoss;

                    if (!IsAlive())
                    {
                        Message += $" and has starved to death after {lifeSpan+1} life units";
                    }else if (IsStarving())
                    {
                        Message += " and is currently starving.";
                    }
                }
                lifeSpan++;
            }
        }

        public void MoveAnimal(Tile curentTile, Tile destination)
        {
            curentTile.MoveAnimalTo(this,destination);
        }

        public override int GetEaten(int consumedNutrients)
        {
            isEaten = true;
            Message = $"{this.GetType().Name} has been eaten after {lifeSpan} life units.";
            return base.GetEaten(consumedNutrients);
        }

        public bool IsAlive()
        {
            return currentEnergy > 0 && !isEaten;
        }

        protected bool IsStarving()
        {
            return currentEnergy <= starvationLevel;
        }

    } 
}
