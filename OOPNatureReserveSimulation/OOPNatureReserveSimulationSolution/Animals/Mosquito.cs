﻿namespace OOPNatureReserveSimulationSolution.Animals
{
    internal class Mosquito : Animal
    {
        private static HashSet<Food> diet = new() { Food.blood, Food.blueberry };
        public Mosquito() : base(3, Food.mosquito, 6, 1) { }

        protected override bool CanEat(Food food)
        {
            return FoodIsContainedInDiet(food, diet);
        }
    }
}
