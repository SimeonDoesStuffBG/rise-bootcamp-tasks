﻿namespace OOPNatureReserveSimulationSolution.Animals
{
    internal class Pike : Animal
    {
        private static HashSet<Food> diet = new() { Food.fish, Food.fly, Food.mosquito };
        public Pike() : base(10, Food.fish, 21, 7) { }

        protected override bool CanEat(Food food)
        {
            return FoodIsContainedInDiet(food, diet);
        }
    }
}
