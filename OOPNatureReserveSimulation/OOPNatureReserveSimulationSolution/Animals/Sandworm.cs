﻿namespace OOPNatureReserveSimulationSolution.Animals
{
    internal class Sandworm : Animal
    {
        private static HashSet<Food> diet = new() { Food.sand, Food.wabbit };
        public Sandworm() : base(0, Food.sandworm, 75, 6) { }

        protected override bool CanEat(Food food)
        {
            return FoodIsContainedInDiet(food, diet);
        }
    }
}
