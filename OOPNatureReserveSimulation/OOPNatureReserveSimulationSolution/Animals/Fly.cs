﻿using System.Reflection.PortableExecutable;

namespace OOPNatureReserveSimulationSolution.Animals
{
    internal class Fly : Animal
    {
        private static HashSet<Food> diet = new() { Food.strawberry, Food.blueberry, Food.grass };

        public Fly() : base(2, Food.fly, 14, 1) { }

        protected override bool CanEat(Food food)
        {
            return FoodIsContainedInDiet(food, diet);
        }
    } 
}
