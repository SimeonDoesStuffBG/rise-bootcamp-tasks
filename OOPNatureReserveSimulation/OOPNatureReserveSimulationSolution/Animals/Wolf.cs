﻿namespace OOPNatureReserveSimulationSolution.Animals
{
    internal class Wolf : Animal
    {
        private Food lastFoodEaten = Food.noFood;
        private static HashSet<Food> diet = new() { Food.beef, Food.mutton, Food.wabbit };
        public Wolf() :base(0, Food.wolf, 20, 5){}

        protected override bool CanEat(Food food)
        {
            return FoodIsContainedInDiet(food, diet) && food!=lastFoodEaten;
        }

        public override void Feed(Edible food)
        {
            if (CanEat(food.type))
            {
                lastFoodEaten = food.type;
            }              
            
            base.Feed(food);
        }
    }
}
