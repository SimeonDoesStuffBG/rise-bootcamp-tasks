﻿namespace OOPNatureReserveSimulationSolution.Animals
{
    internal class Wabbit : Animal//Shhh! Be vewy vewy quiet! I'm making wabbits.
    {
        private static HashSet<Food> diet = new() { Food.grass, Food.strawberry, Food.algae };
        public Wabbit():base(40, Food.wabbit, 83, 8) { }

        protected override bool CanEat(Food food)
        {
            return FoodIsContainedInDiet(food, diet);
        }

    }
}
