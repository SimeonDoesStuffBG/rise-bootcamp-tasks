﻿namespace OOPNatureReserveSimulationSolution.Animals
{
    internal class Sheep :Animal
    {
        private static HashSet<Food> diet = new() { Food.grass, Food.rose, Food.apple };
        public Sheep():base(13,Food.mutton, 26, 3) { }

        protected override bool CanEat(Food food)
        {
            return FoodIsContainedInDiet(food, diet);
        }
    }
}
