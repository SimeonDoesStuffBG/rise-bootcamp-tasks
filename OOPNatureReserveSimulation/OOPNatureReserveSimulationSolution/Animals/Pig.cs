﻿namespace OOPNatureReserveSimulationSolution.Animals
{
    internal class Pig : Animal
    {
        private static HashSet<Food> diet = new() { Food.strawberry, Food.blueberry, Food.pork };
        public Pig() : base(32, Food.pork, 69, 5) { }

        protected override bool CanEat(Food food)
        {
            return FoodIsContainedInDiet(food, diet);
        }
    }
}
