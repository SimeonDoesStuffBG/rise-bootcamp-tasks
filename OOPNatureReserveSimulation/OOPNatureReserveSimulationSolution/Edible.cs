﻿namespace OOPNatureReserveSimulationSolution
{
    public enum Food
    {
        noFood = -1,
        plantsStart = 0,
        strawberry = 0, 
        apple, 
        grass, leaves, blueberry, rose, algae,
        plantsEnd,
        animalsStart = 100,
        beef = 100, mutton, frog, pork, wabbit, fly, mosquito, fish, cat, bear, sandworm, wolf, //meat
        animalsEnd,
        otherStart = 200,
        honey = 200, sand, blood,
        otherEnd
    }
 
    internal abstract class Edible
    {
        public readonly Food type; 
        protected readonly int maxNutrients;
        protected int currentNutrients;

        public int CurrentNutrients { get => currentNutrients; }
        public Edible(int maxNutrients, Food type) 
        {
            this.maxNutrients = maxNutrients;
            currentNutrients = maxNutrients;
            this.type = type;
        }
        public virtual int GetEaten(int consumedNutrients)
        {
            if(consumedNutrients > currentNutrients)
            {
                consumedNutrients = currentNutrients;
            }
            currentNutrients -= consumedNutrients;

            return consumedNutrients;
        }

        public virtual bool HasNutrients()
        {
            return currentNutrients > 0;
        }
    }
}
