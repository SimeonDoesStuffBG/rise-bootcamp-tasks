﻿namespace OOPNatureReserveSimulationSolution.Plants
{
    internal class Rose : Plant
    {
        public Rose() : base(12, Food.rose, 1) { }
    }
}
