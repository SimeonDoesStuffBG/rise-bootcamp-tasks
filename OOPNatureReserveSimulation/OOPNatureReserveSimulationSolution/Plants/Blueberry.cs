﻿namespace OOPNatureReserveSimulationSolution.Plants
{
    internal class Blueberry : Plant
    {
        public Blueberry() : base(31, Food.blueberry, 3){}
    }
}
