﻿namespace OOPNatureReserveSimulationSolution.Plants
{
    internal class Grass : Plant
    {
        public Grass() :base(20,Food.grass, 3)
        { }
    }
}
