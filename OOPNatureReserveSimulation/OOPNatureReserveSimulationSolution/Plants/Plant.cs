﻿namespace OOPNatureReserveSimulationSolution.Plants
{
    internal abstract class Plant : Edible
    {
        
        private readonly int regenerationRate;

        public Plant(int maxNutrients,Food type, int regenerationRate):base(maxNutrients,type)
        {
            this.regenerationRate = regenerationRate;
        }

        public void Regenerate()
        {
            currentNutrients = Math.Min(currentNutrients + regenerationRate, maxNutrients);
        }

        public override bool HasNutrients()
        {
            return true;
        }
    }
}
