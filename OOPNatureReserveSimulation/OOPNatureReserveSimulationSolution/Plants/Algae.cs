﻿namespace OOPNatureReserveSimulationSolution.Plants
{
    class Algae : Plant
    {
        public Algae() : base(13, Food.algae, 1) { }
    }
}
