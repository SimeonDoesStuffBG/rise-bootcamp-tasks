﻿namespace OOPNatureReserveSimulationSolution.Plants
{
    internal class Weed : Plant
    {
        public Weed() : base(42, Food.leaves, 7) { }
    }
}
