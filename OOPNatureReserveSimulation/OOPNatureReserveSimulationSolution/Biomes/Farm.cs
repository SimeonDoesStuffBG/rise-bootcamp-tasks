﻿using OOPNatureReserveSimulationSolution.Animals;
using OOPNatureReserveSimulationSolution.Plants;

namespace OOPNatureReserveSimulationSolution.Biomes
{
    internal class Farm : Biome//Old McDonald had a farm EIEIO
    {
        private static Food[] animals = { Food.pork, Food.cat, Food.mutton, Food.wolf, Food.beef, Food.wabbit };

        private static Func<Animal>[] animalFactories =
        {
            ()=>new Pig(),
            ()=>new Cat(),
            ()=>new Sheep(),
            ()=>new Wolf(),
            ()=>new Cow(),
            ()=>new Wabbit()
        };



        private static Farm instance = null;
        private Farm() : base(6, 5, 4) { }

        public static Farm Instance()
        {
            if (instance == null)
            {
                instance = new Farm();
            }

            return instance;
        }
        public override bool AnimalLivesHere(Animal animal)
        {
            throw new NotImplementedException();
        }

        public override Animal CreateAnimal()
        {
            throw new NotImplementedException();
        }

        public override Edible CreateOtherFood()
        {
            throw new NotImplementedException();
        }

        public override Plant CreatePlant()
        {
            throw new NotImplementedException();
        }
    }
}
