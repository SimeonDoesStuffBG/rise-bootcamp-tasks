﻿using OOPNatureReserveSimulationSolution.Animals;
using OOPNatureReserveSimulationSolution.Plants;
using OOPNatureReserveSimulationSolution;

namespace OOPNatureReserveSimulationSolution.Biomes
{
    internal abstract class Biome
    {
        static Random random = new Random();

        public readonly int animals;
        public readonly int plants;
        public readonly int otherFoods;

        protected Biome(int animals, int plants, int otherFoods)
        {
            this.animals = animals;
            this.plants = plants;
            this.otherFoods = otherFoods;
        }
        protected static bool AnimalCanLiveIn(Animal animal, HashSet<Food> animalsLivingInBiome)
        {
            return animalsLivingInBiome.Contains(animal.type);
        }

        protected static T ExecuteRandomFuncFromList<T>(Func<T>[] factory)
        {
            int index = random.Next(factory.Length);

            T result = factory[index]();

            return result;
        }

        public abstract bool AnimalLivesHere(Animal animal);
        public abstract Animal CreateAnimal();
        public abstract Plant CreatePlant();
        public abstract Edible CreateOtherFood();
    }
}
