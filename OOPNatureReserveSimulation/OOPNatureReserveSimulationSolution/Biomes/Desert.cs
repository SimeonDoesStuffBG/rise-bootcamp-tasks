﻿using OOPNatureReserveSimulationSolution.Animals;
using OOPNatureReserveSimulationSolution.OtherFoods;
using OOPNatureReserveSimulationSolution.Plants;

namespace OOPNatureReserveSimulationSolution.Biomes
{
    internal class Desert : Biome
    {
        private static HashSet<Food> animalsCollection = new() { Food.sandworm, Food.wabbit };
        private static Desert instance = null;

        private static Func<Animal>[] animalFactories = 
        {
            ()=>new Sandworm(),
            ()=>new Wabbit()
        };

        private static Func<Plant>[] plantFactories =
        {
            ()=>new Grass()
        };

        private static Func<Edible>[] otherFoodFactories =
        {
            ()=>new Sand()
        };
        
        private Desert() : base(4,3,5)
        {

        }

        public static Desert Instance()
        {
            if (instance == null)
            {
                instance = new Desert();
            }

            return instance;
        }
        
        public override Animal CreateAnimal()
        {
            return ExecuteRandomFuncFromList(animalFactories);
        }

        public override Edible CreateOtherFood()
        {
            return ExecuteRandomFuncFromList(otherFoodFactories);
        }

        public override Plant CreatePlant()
        {
            return ExecuteRandomFuncFromList(plantFactories);
        }

        public override bool AnimalLivesHere(Animal animal)
        {
            return AnimalCanLiveIn(animal, animalsCollection);
        }
    }
}
